# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is the repository of a programming test for an full stack developer job at Glory.

________________________________________________________________________________________________________________________________
This test forms part of the hiring process for Glory Software engineering candidates.
 

The text file Train Data.txt (attached) contains the details of trains departing from a train station.
 
Write a program in Java that loads Train Data.txt and provides the following features:
 
1) Lists all trains displaying destination and departure time
2) Enables a train to be selected and a window or aisle seat to be booked
3) In the event a desired seat (window/aisle) is not available, offers the option of booking another available seat
4) If no seats are available, offers the option of picking a different train to a different destination
 

Assumptions:
 
1) You can use any user interface technology e.g. Console Application, SWT, Swing, …

2) The fields in the text file are: Departure Time, Destination, total number of seats

3) The total number of seats can be assumed to be an equal split between window and aisle. For example, 12 total means 6 window and 6 aisle seats.

4) The solution should be provided in a .zip file. The .zip file should contain all files necessary to build the solution. This includes source code but also all third-party Java libraries. It is not necessary to provide libraries that are part of Java 11. 

5) The solution must contain build files to build it either in the Eclipse IDE, Ant or Maven.


### How do I get set up? ###

* Summary of set up
This project uses Angular.js for front end, and Spring Boot Web Service for the backend. The H2 in memory database is used to store the data imported from a text file. 



* Configuration
You can change the project config at the application.properties file
* Dependencies
Angular.js, Spring Boot( with an embedded Tomcat server )
* Database configuration
H2 database is accessible from the endpoint: http://localhost:8080/nowhere/h2/ , no password needed. 
Since H2 is a in-memory database, once application shuts down, the database will be gone too. 
* How to run tests
You can run the jUnit test cases
* Deployment instructions
To run the application in two ways:

1. Go to the Spring boot entry point class TrainBookingSystemApplication.java, run it as a regular java application
2. Using manven to run it through command line. Open your console window, cd into the project folder ,type in "mvn springboot:run" at the command line. 

The public facing url for the application is http://localhost:8080/nowhere

The API endpoints can be accessed at  http://localhost:8080/nowhere/api

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner: Michael Kong
* Other community or team contact