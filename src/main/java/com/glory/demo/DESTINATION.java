package com.glory.demo;

/* Since the destinations of every train station are known and change very little over the time, store them as enum to prevent user typos */
public enum DESTINATION { 
	CHICAGO("Chicago"), 
	CLEVELAND("Cleveland"), 
	INDIANAPOLIS("Indianapolis"), 
	CINCINNATI("Cincinnati"), 
	WASHINGTON("Washington"), 
	PHILADELPHIA("Philadelphia"), 
	PORTLAND("Portland"),
	NEW_YORK("New York"), 
	TAMPA("Tampa"), 
	DURHAM("Durham"), 
	SAVANNAH("Savannah"), 
	JACKSONVILLE("Jacksonville"),
	SEATTLE("Seattle"),
	SAN_FRANCISCO("San Francisco"),
	SAN_DIEGO("San Diego"),
	LOS_ANGELES("Los Angeles"),
	ORLANDO("Orlando"),
	NOWHERE("Nowhere"); //default if no destination city is provided
	
	private String cityName; //display name
	
	DESTINATION(String cityName){
		this.cityName = cityName;
	}

	public String getCityName() {
		return cityName;
	}
	
	@Override
	public String toString() {
		return cityName;
	}
	
	//check if a city is a train destination
	public static boolean contains(String city) { 

		for (DESTINATION d : DESTINATION.values()) {
			if (d.cityName.equalsIgnoreCase(city)) {
				return true;
			}
		}

		return false;
	}
	
	public static DESTINATION fromString( String city) {
		for (DESTINATION d : DESTINATION.values()) {
			if (d.cityName.equalsIgnoreCase(city)) {
				return d;
			}
		}
		throw new IllegalArgumentException("No matching with city " + city + " found");

	}
};
