package com.glory.demo;

import java.time.LocalTime;

import javax.persistence.*;
import org.hibernate.annotations.GenericGenerator;//for uuid

@Entity
@Table(name="train")
public class DepartureTrain {
	
	@Id
    @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid2")
    @Column(name = "id", unique = true, nullable = false)
	private String id;
	
	@Column(name="window_seat")
	private int windowSeatAvailable;
	
	@Column(name="aisle_seat")
	private int aisleSeatAvailable;
	
	@Column(name="departure_time")
	private LocalTime departureTime;
	
	
	@Column(name="destination")
	private String destination;
	
	//Constructors
    public DepartureTrain(){} //default constructor for JPA
    
	public DepartureTrain(String destination, LocalTime departureTime, int windowSeatAvailable, int aisleSeatAvailable){
		this.windowSeatAvailable = windowSeatAvailable;
		this.aisleSeatAvailable = aisleSeatAvailable;
		this.departureTime = departureTime;
		this.destination = destination;
	}

	public String getId() {
		return id;
	}

	
	public int getSeatsAvailable() {
		return windowSeatAvailable + aisleSeatAvailable;
	}
	
	public int getWindowSeatAvailable() {
		return windowSeatAvailable;
	}
	
	public int getAisleSeatAvailable() {
		return aisleSeatAvailable;
	}

	public LocalTime getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(LocalTime departureTime) {
		this.departureTime = departureTime;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public void setWindowSeatAvailable(int windowSeatAvailable) {
		this.windowSeatAvailable = windowSeatAvailable;
	}
	
	public void setAisleSeatAvailable(int aisleSeatAvailable) {
		this.aisleSeatAvailable = aisleSeatAvailable;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((departureTime == null) ? 0 : departureTime.hashCode());
		result = prime * result + ((destination == null) ? 0 : destination.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DepartureTrain other = (DepartureTrain) obj;
		if (departureTime == null) {
			if (other.departureTime != null)
				return false;
		} else if (!departureTime.equals(other.departureTime))
			return false;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DepartureTrain [seatsAvailable=" + windowSeatAvailable+ aisleSeatAvailable + ", departureTime=" + departureTime
				+ ", destination=" + destination + "]";
	}
	
	

}
