package com.glory.demo;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;



@Configuration
public class RepositoryConfiguration implements RepositoryRestConfigurer {

	//by default, Sprint Boot does not expose the id to the JSON, this make sure we have an id
	 @Override
	    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
	        config.exposeIdsFor(DepartureTrain.class);
	    }
}