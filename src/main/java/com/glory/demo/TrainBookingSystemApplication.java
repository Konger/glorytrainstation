package com.glory.demo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

@SpringBootApplication
@ComponentScan("com.glory.demo")
public class TrainBookingSystemApplication {

	@Autowired
	private ResourceLoader resourceLoader;
	
	@Autowired
	private TrainRepository repository;

	@EventListener(ApplicationReadyEvent.class)
	public void loadDataAfterStartup() { //init the application
	    System.out.println("Load data into the application...");
	    Resource resource  = resourceLoader.getResource("classpath:\\static\\Train Data.txt");
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
			String currentLine;
			
			while((currentLine = reader.readLine()) != null) {
				currentLine.trim();//remove whitespace from both ends
				String[] elements = currentLine.split(",");
				String time = elements[0].replace('.', ':').trim();
				String city = elements[1].trim();
				String seats= elements[2].trim();
				int seatsAvailable;
				int windowSeatsAvailable;
				int aisleSeatsAvailable;
				try {
					seatsAvailable = Integer.parseInt(seats);
					if(seatsAvailable%2==0) {
						windowSeatsAvailable = aisleSeatsAvailable = seatsAvailable/2; 
					}
					else {
						aisleSeatsAvailable = (seatsAvailable+1)/2;
						windowSeatsAvailable = (seatsAvailable+1)/2-1;
					}
				} catch(NumberFormatException e) {
					System.out.println(seats + " is not a valid seats count. Please check your input data for typos.");
					continue;
				}
				LocalTime departureTime;
				try {
					departureTime = LocalTime.parse(time);
				} catch(DateTimeParseException e) {
					System.out.println(time + " is not a valid departure time. Please check your input data for typos.");
					continue;
				}
				if(DESTINATION.contains(city)){
					DESTINATION destination = DESTINATION.fromString(city);
					DepartureTrain departureTrain = new DepartureTrain(destination.getCityName() ,departureTime, windowSeatsAvailable, aisleSeatsAvailable );
					repository.save(departureTrain);
				}
				else {
					
					System.out.println(city + " is not a valid destination. Please check your input data for typos.");
					continue;
				}
				
			}//end of while
			
			
		} catch (IOException e) {
			System.out.println("Unable to open data input file.");
		}
	}
	
	public static void main(String[] args) {
		SpringApplication.run(TrainBookingSystemApplication.class, args);
	}

}
