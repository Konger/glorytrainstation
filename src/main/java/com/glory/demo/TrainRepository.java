package com.glory.demo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


@RepositoryRestResource(collectionResourceRel = "trains", path = "trains")
public interface TrainRepository extends CrudRepository<DepartureTrain, String> {

}
