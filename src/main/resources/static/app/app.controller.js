/**
 * 
 */
angular.
	module("app").
	controller('mainController', ['$scope', '$location', '$log', '$http', '$document', '$rootScope', function($scope, $location, $log, $http, $document, $rootScope) {
    console.log("At mainController");
	//Global values
	$rootScope.NOWHERE_URL = "/nowhere/";
	$rootScope.NOWHERE_API_URL = "/nowhere/api/";
	
	$scope.showBooking=[];
	
	$http({
		method: 'GET',
		url:  $rootScope.NOWHERE_API_URL + 'trains'
	}).then( function(result) {
		$scope.trains = result.data._embedded.trains;
	}, function(error) {
		$log(error);
	});
	
	$scope.showBookingOptions = function($index){
		$scope.showBooking[$index] = true;
	}
	
	//To book window seats
	$scope.bookingWindow = function(train){
		if(train.windowSeatAvailable>0){
			train.windowSeatAvailable--;
			train.seatsAvailable--;
		}
	}
	
	//To book aisle seats
	$scope.bookingAisle = function(train){
		if(train.aisleSeatAvailable>0){
			train.aisleSeatAvailable--;
			train.seatsAvailable--;
		}
	}
	
	$scope.confirmBooking = function(train, $index){
	    console.log(train);
		$http({
			method: 'PUT',
			url:  $rootScope.NOWHERE_API_URL + 'trains/' +  train.id, 
			data: train
		}).then( function(result) {
			train = result.data;
			$scope.showBooking[$index] = false;
		}, function(error) {
			$log(error);
		});
	}
	
}]);


angular.
module("app").
controller('bookingController', ['$scope', '$location', '$log', function($scope, $location, $log) {
	console.log("At bookingController");
$log.info( $location.path() );


}]);

