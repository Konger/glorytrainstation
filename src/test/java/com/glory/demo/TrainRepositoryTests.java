package com.glory.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@Transactional
@DataJpaTest
public class TrainRepositoryTests {

	@Autowired
    private TrainRepository repository;
	
	public void testCreate() throws Exception {
		DepartureTrain train = new DepartureTrain();
		Long counterBeforeInsert = this.repository.count();
		assertEquals(Long.valueOf(0), counterBeforeInsert);
		this.repository.save(train);
	    Long counterAfterInsert = this.repository.count();
	    assertEquals(Long.valueOf(1), counterAfterInsert);
	    
	}
	
	

	
}
